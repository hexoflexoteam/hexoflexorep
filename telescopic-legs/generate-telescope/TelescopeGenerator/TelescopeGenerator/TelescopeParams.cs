﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TelescopeGenerator
{
    public class TelescopeParams
    {
        public int NumberOfSections = 3;
        public double OuterRingDiameter = 24;
        public double WallThickness = 2;
        public double ChannelWidth = 8;
        public double RingHeight = 50;
        public double ChannelClearance = 2;

        public double Clearance = 0.65;
        public double KeyHeight = 2;
        //TODO: Constructor

        public double KeyLength
        {
            get
            {
                return WallThickness * 0.5;
            }
        }
    }
}
