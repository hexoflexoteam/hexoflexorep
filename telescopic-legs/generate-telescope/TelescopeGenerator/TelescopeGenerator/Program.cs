﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Reflection;
using System.IO;

using AI = Inventor;

namespace TelescopeGenerator
{
    class Program
    {
        static void Main(string[] args)
        {
            AI.Application invApp = null;

            try
            {
                invApp = GetInventorInstance();
            }
            catch
            {
                Console.WriteLine("Unable to get inventor instance");
            }

            //TODO: specify output path on command line
            string exeDir = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + @"\";
            string buildPath = exeDir;
            string templatePath = exeDir + @"Templates\";

            TelescopeAssembler assmebler = new TelescopeAssembler(invApp, buildPath, templatePath);
            assmebler.Assemble();

            Console.WriteLine("Built!");
        }

        static Inventor.Application GetInventorInstance()
        {
            Inventor.Application invApp;

            try
            {
                invApp = (Inventor.Application)Marshal.GetActiveObject("Inventor.Application");
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get or start Inventor", ex);
            }

            return invApp;
        }
    }
}

