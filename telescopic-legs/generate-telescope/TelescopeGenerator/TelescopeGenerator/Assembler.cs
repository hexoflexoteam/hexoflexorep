﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using AI=Inventor;

namespace TelescopeGenerator
{
    public class TelescopeAssembler
    {
        private Inventor.Application _invApp;

        //TODO: construct this
        public TelescopeParams Telescope = new TelescopeParams();

        private string _buildPath;

        private string _assemblyName
        {
            get
            {
                return "telescope.iam";
            }
        }

        private string _templatePath;

        public string OuterSectionFilepath
        {
            get
            {
                return _templatePath + "telescopic_tube_outer.ipt";
            }
        }

        public string MiddleSectionFilepath
        {
            get
            {
                return _templatePath + "telescopic_tube_middle.ipt";
            }
        }

        private AI.AssemblyDocument _assDoc;


        public TelescopeAssembler(Inventor.Application invApp, string buildPath, string templatePath)
        {
            _invApp = invApp;
        
            if (!Directory.Exists(buildPath))
            {
                throw new Exception("Build path does not exist: " + buildPath);
            }

            _buildPath = buildPath;

            if (!Directory.Exists(templatePath))
            {
                throw new Exception("Template path does not exist: " + templatePath);
            }
            _templatePath = templatePath;
        }

        public void Assemble()
        {
            //Close existing assembly
            string assemblyPath = _buildPath + _assemblyName;
            if (File.Exists(assemblyPath))
            {
                File.Delete(assemblyPath);
            }

            foreach (AI._Document openDoc in _invApp.Documents)
            {
                try
                {
                    if (openDoc.FullFileName == assemblyPath)
                    {
                        openDoc.Close(true);
                    }
                }
                catch (Exception e) { }
            }

            // Create a new assembly document.
            _assDoc = (AI.AssemblyDocument)_invApp.Documents.Add(
                AI.DocumentTypeEnum.kAssemblyDocumentObject,
                _invApp.FileManager.GetTemplateFile(AI.DocumentTypeEnum.kAssemblyDocumentObject)
            );
            _assDoc.SaveAs(assemblyPath, false);

            //Make outer section
            var outerSection = CopyTemplate(OuterSectionFilepath, "outer_section");
            UpdateParameter(outerSection, "RingDiameter", Telescope.OuterRingDiameter.ToString());

            double R = Telescope.OuterRingDiameter;
            double dR = 2 * (Telescope.WallThickness + Telescope.Clearance);
            for(int i = 0; i < Telescope.NumberOfSections - 1; i++)
            {
                R = R - dR;

                var middleSection = CopyTemplate(MiddleSectionFilepath, "middle_section_" + i);
                UpdateParameter(middleSection, "RingDiameter", R.ToString());
                UpdateParameter(middleSection, "KeyRadialLength", Telescope.KeyLength.ToString());
            }

            foreach(AI.PartDocument doc in _assDoc.AllReferencedDocuments)
            {
                UpdateParameter(doc, "WallThickness", Telescope.WallThickness.ToString());
                UpdateParameter(doc, "ChannelWidth", Telescope.ChannelWidth.ToString());
                UpdateParameter(doc, "RingHeight", Telescope.RingHeight.ToString());
                UpdateParameter(doc, "ChannelClearance", Telescope.ChannelClearance.ToString());
            }
        }

        private void UpdateParameter(AI.PartDocument doc, string property, string newVal)
        {
            var userParams = doc.ComponentDefinition.Parameters.UserParameters;

            userParams[property].Expression = newVal;

            _assDoc.Update();
        }

        private AI.PartDocument CopyTemplate(string filepath, string name)
        {
            string outputPath = _buildPath + name + ".ipt";
            File.Copy(filepath, outputPath, overwrite: true);
            return AddPart(outputPath, name);
        }

        private AI.PartDocument AddPart(string filepath, string name)
        {
            AI.Matrix matrix = _invApp.TransientGeometry.CreateMatrix();

            AI.ComponentOccurrence occ = _assDoc.ComponentDefinition.Occurrences.Add(filepath, matrix);
            occ.Name = name;

            return occ.Definition.Document;
        }
    }
}
