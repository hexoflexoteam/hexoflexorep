﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class MiddleSectionParams : TelescopeParams
    {
        [PartParameter]
        public double KeyClearance = 0.75;

        [PartParameter]
        public double KeyHeight = 2;

        [PartParameter]
        public double KeyRadialLength
        {
            get
            {
                //Maintains length whilst maintaining clearance
                return WallThickness * 0.5;
            }
        }

        public MiddleSectionParams(AI.PartDocument aiDoc) : base(aiDoc) { }

        public MiddleSectionParams(AI.PartDocument aiDoc, TelescopeParams nextBiggestTelescope) : base(aiDoc)
        {
            WallThickness = nextBiggestTelescope.WallThickness;
            RingDiameter = nextBiggestTelescope.RingDiameter - 2 * (WallThickness + KeyClearance);
        }
    }
}
