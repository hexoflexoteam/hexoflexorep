﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class Builder
    {
        //TODO: Should this really be static?
        public static double PrintingTolerance = 0.7;

        //TODO: put this as a command line arg
        private string _rootFolder = @"C:\Users\af8291\Google Drive\PhD\Robosoft\Hexo-Flexo-Bot\Parts\Telecopic tube\Inventor\V3\";

        private AI.Application _aiApp;

        public Builder()
        {
            try
            {
                _aiApp = (AI.Application)Marshal.GetActiveObject("Inventor.Application");
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to get or start Inventor", ex);
            }
        }

        public void Build()
        {
            UpdateTelescope();
        }

        public void UpdateTelescope()
        {
            //Big Plug
            AI.PartDocument bigPlug = (AI.PartDocument)_aiApp.Documents.Open(_rootFolder + "PlugBigHole.ipt");
            PlugBigHoleParams bigPlugParams = new PlugBigHoleParams(bigPlug);
            bigPlugParams.Update();
            
            //Default outer
            //TODO: Design the telescope so it doesn't all print at once, we should be able to get it as tight as printer tolerances then...
            AI.PartDocument outerSection = (AI.PartDocument)_aiApp.Documents.Open(_rootFolder + "OuterSection.ipt");
            OuterSectionParams outerSectionParams = new OuterSectionParams(outerSection, bigPlugParams);
            outerSectionParams.Update();

            //Middle Ring
            AI.PartDocument middleSection = (AI.PartDocument)_aiApp.Documents.Open(_rootFolder + "MiddleSection.ipt");
            MiddleSectionParams middleSectionParams = new MiddleSectionParams(middleSection, outerSectionParams);
            middleSectionParams.Update();

            //Inner ring
            AI.PartDocument innerSection = (AI.PartDocument)_aiApp.Documents.Open(_rootFolder + "InnerSection.ipt");
            InnerSectionParameters innerSectionParams = new InnerSectionParameters(innerSection, middleSectionParams);
            innerSectionParams.Update();

            //Small Plug Hole
            AI.PartDocument plugSmall = (AI.PartDocument)_aiApp.Documents.Open(_rootFolder + "PlugSmallHole.ipt");
            PlugSmallHole plugSmallParams = new PlugSmallHole(plugSmall, innerSectionParams);
            plugSmallParams.Update();


        }
    }
}
