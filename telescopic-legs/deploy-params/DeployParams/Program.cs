﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                new Builder().Build();
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("-------------------");
                Console.WriteLine("Build Failed!!!");
                return;
            }

            Console.WriteLine("-------------------");
            Console.WriteLine("Build Successful");
        }

    }
}
