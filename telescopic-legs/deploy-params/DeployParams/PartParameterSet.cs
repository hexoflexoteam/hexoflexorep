﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;

using AI = Inventor;


//There is an assumption all properties/fields are properties!
//TODO: use custom label thing
namespace DeployParams
{
    public abstract class PartParameterSet
    {
        private AI.PartDocument _aiDoc;

        public PartParameterSet(AI.PartDocument aiDoc)
        {
            //TODO: handling if document isn't open
            _aiDoc = aiDoc;
        }

        public void Update()
        {
            Console.WriteLine("------------");
            Console.WriteLine("Updating Parameters for: " + _aiDoc.FullFileName);

            foreach (MemberInfo member in GetType().GetMembers().Where(x => MemberIsPartParameter(x)))
            {
                //TODO: handling if document isn't open
                var userParams = _aiDoc.ComponentDefinition.Parameters.UserParameters;

                string value = "";
                switch (member.MemberType)
                {
                    case MemberTypes.Field:
                        value = ((FieldInfo)member).GetValue(this).ToString();
                        break;
                    case MemberTypes.Property:
                        value = ((PropertyInfo)member).GetValue(this).ToString();
                        break;
                    default:
                        throw new NotImplementedException();
                }

                Console.WriteLine(member.Name + ": " + value);

                try
                {
                    userParams[member.Name].Expression = value;
                }
                catch(Exception e)
                {
                    string message = "Failed to add param '" + member.Name + "' to " 
                        + _aiDoc.FullFileName + " does it exist in the file?";
                    throw new Exception(message, e);
                }
            }

            _aiDoc.Update();
            _aiDoc.Save();
        }

        private bool MemberIsPartParameter(MemberInfo member)
        {
            return member.CustomAttributes.Any(x => x.AttributeType == typeof(PartParameter));
        }
    }
}
