﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class PlugBigHoleParams : PartParameterSet
    {
        [PartParameter]
        public double PluggingLength = 16;

        [PartParameter]
        public double BottomClearance = 2;

        [PartParameter]
        public double LockSlotDiameter = 24;

        [PartParameter]
        public double KeyLength = 2;

        [PartParameter]
        public double KeyDepth = 1.5;

        public PlugBigHoleParams(AI.PartDocument aiDoc) : base(aiDoc) { }
    }
}
