﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class OuterSectionParams : TelescopeParams
    {
        [PartParameter]
        public double OuterChannelDepth;//= 20;

        [PartParameter]
        public double OuterChannelSweep = 30;

        public OuterSectionParams(AI.PartDocument aiDoc) : base(aiDoc) { }

        public OuterSectionParams(AI.PartDocument aiDoc, PlugBigHoleParams bigPlug) : base(aiDoc)
        {
            OuterChannelDepth = bigPlug.PluggingLength - bigPlug.BottomClearance - Builder.PrintingTolerance;
            RingDiameter = bigPlug.LockSlotDiameter - Builder.PrintingTolerance;
            WallThickness = 2*bigPlug.KeyDepth;
        }
    }
}
