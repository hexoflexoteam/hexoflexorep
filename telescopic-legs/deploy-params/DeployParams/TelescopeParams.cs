﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AI = Inventor;

namespace DeployParams
{
    public abstract class TelescopeParams : PartParameterSet
    {
        //Default for the outer ring
        [PartParameter]
        public double RingDiameter; //= 24;

        [PartParameter]
        public double WallThickness; //= 4;

        [PartParameter]
        public double RingHeight = 50;

        [PartParameter]
        public double ChannelClearance = 2;

        [PartParameter]
        public double ChannelWidth = 4;

        public TelescopeParams(AI.PartDocument aiDoc) : base(aiDoc) { }
    }
}
