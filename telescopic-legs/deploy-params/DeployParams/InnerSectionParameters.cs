﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class InnerSectionParameters : MiddleSectionParams
    {
        [PartParameter]//10/30
        public double OuterChannelDepth = 10;

        [PartParameter]
        public double OuterChannelSweep = 30;

        //TODO: Can I inherit this constructor?
        public InnerSectionParameters(AI.PartDocument aiDoc, TelescopeParams nextBiggestTelescope) : base(aiDoc)
        {
            WallThickness = nextBiggestTelescope.WallThickness;
            RingDiameter = nextBiggestTelescope.RingDiameter - 2 * (WallThickness + KeyClearance);
        }
    }
}
