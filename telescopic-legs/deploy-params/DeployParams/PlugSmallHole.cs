﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using AI = Inventor;

namespace DeployParams
{
    public class PlugSmallHole : PartParameterSet
    {
        //TODO: THIS IS NOT EDITABLE, BUILT ON TOP OF PLINIO'S CAD
        [PartParameter]
        public double PluggingLength = 12;

        [PartParameter]
        public double LipDepth = 2;

        [PartParameter]
        public double BottomClearance = 2;

        [PartParameter]
        public double LockSlotDiameter;

        [PartParameter]
        public double KeyLength = 2;

        [PartParameter]
        public double KeyDepth; // 1.5

        public PlugSmallHole(AI.PartDocument aiDoc, InnerSectionParameters innerSection) : base(aiDoc)
        {
            LockSlotDiameter = innerSection.RingDiameter + Builder.PrintingTolerance;
            KeyDepth = innerSection.WallThickness / 2;
            
            // Min thickness of 2mm
            LipDepth = new double[2] {2,  innerSection.OuterChannelDepth + BottomClearance - PluggingLength}.Max();
        }
    }
}
