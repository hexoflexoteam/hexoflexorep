#include "leg.h"

Leg front_r;
Leg front_l;
Leg mid_l;
Leg mid_r;
Leg back_l;
Leg back_r;

void setup() 
{
  front_r.attach(0, 1); 
  front_r.SetO(45);
  front_r.SetDown(110);

  front_l.attach(2, 3);
  front_l.SetO(45);
  front_l.SetDown(45);
  front_l.changeDir();

  mid_l.attach(4, 5);
  mid_l.SetDown(120);
  mid_l.SetO(-10);

  mid_r.attach(6, 7);
  mid_r.SetDown(130);
  mid_r.SetO(0);
  mid_r.changeDir();

  back_r.attach(8, 9);
  back_r.SetDown(60);
  back_r.SetO(50);

  back_l.attach(10, 11);
  back_l.SetDown(90);
  back_l.SetO(-20);
  back_l.changeDir();
  

}

void loop() 
{
  front_r.walk();
  front_l.walk();
  mid_l.walk();
  mid_r.walk();
  back_r.walk();
  back_l.walk();
}

