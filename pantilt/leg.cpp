#if defined(ARDUINO) && ARDUINO >= 100
  #include "Arduino.h"
#else
  #include "WProgram.h"
  #include <pins_arduino.h>
#endif
#include "Leg.h"
#include <Servo.h>

Leg::Leg()
{
	//-- Initialization of oscilaltor parameters
	_TS=30;
	_T=2000;
	setInc();

	_previousMillis=0;

	//-- Default parameters
	_down = 90;
	dir = 1;
	_RA=30;
	_VA=40;
	_phase=0;
	_phase0=0;
	_O=0;
	_stop=false;
	_state = 0;
}

void Leg::reset()
{
	_phase = 0;
	_Vservo.write(_down);
	_Rservo.write(90);
}

void Leg::setInc()
{
	_N = _T/_TS;
	_inc = 2*M_PI/_N;
}

//-- This function returns true if another sample
//-- should be taken (i.e. the TS time has passed since
//-- the last sample was taken
bool Leg::next_sample()
{
	//-- Read current time
	_currentMillis = millis();
 	//-- Check if the timeout has passed
 	if(_currentMillis - _previousMillis > _TS) 
 	{
		_previousMillis = _currentMillis;   
		return true;
  	}

  	return false;
}

//-- Attach an Leg to a servo
//-- Input: pin is the arduino pin were the servo
//-- is connected
void Leg::attach(int pinR, int pinV )
{
	//-- If the Leg is detached, attach it.
	if(!_Vservo.attached())
	{
		//-- Attach the servo and move it to the home position
		_Vservo.attach(pinV);
		_Vservo.write(_down);
  	}

  	if(!_Rservo.attached())
  	{
		_Rservo.attach(pinR);
		_Rservo.write(90);
  	}      
}

//-- Detach an Leg from his servo
void Leg::detach()
{
	//-- If the Leg is attached, detach it.
	if(_Vservo.attached())
  	{
		_Vservo.detach();
  	}

  	if(_Rservo.attached())
  	{
		_Rservo.detach();
  	}
}

/*************************************/
/* Set the Leg period, in ms  */
/*************************************/
void Leg::SetT(unsigned int T)
{
  	//-- Assign the new period
  	_T=T;
  
  	//-- Recalculate the parameters
  	setInc();
}

/***********************************/
/* Set update frequency            */
/***********************************/
void Leg::SetTS(unsigned int TS)
{
  	_TS=TS;
  	setInc();
}

/*******************************/
/* Manual set of the position  */
/******************************/
void Leg::SetPosition(int positionV, int positionR)
{
  	_Rservo.write(positionR);
  	_Vservo.write(positionV);
}

void Leg::Up()
{
  	_Vservo.write(_down + _VA);
}

void Leg::Down()
{
  	_Vservo.write(_down);
}

/*******************************************************************/
/* This function should be periodically called                     */
/* in order to maintain the oscillations. It calculates            */
/* if another sample should be taken and position the servo if so  */
/*******************************************************************/
void Leg::swing()
{
  	if (next_sample())
  	{
		if (!_stop)
		{
	  		_Vservo.write(_down+_VA);
	  		_Rpos = round(_RA * sin(_phase + _phase0) + _O);
	  		_Rservo.write(_Rpos+90);
	  		_phase = _phase + _inc;
		}
  	}
}

void Leg::stance()
{
	if (next_sample())
	{
		if (!_stop)
		{
			_Vservo.write(_down);
			_Rpos = round(_RA * sin(_phase + _phase0) + _O);
			_Rservo.write(_Rpos+90);
			_phase = _phase + _inc;
		}
	}
}

void Leg::walk()
{
	//-- Only When TS milliseconds have passed, the new sample is obtained
	if (next_sample()) 
	{
	  //-- If the Leg is not stopped, calculate the servo position
	  	if (!_stop) 
	  	{
			//-- Sample the sine function and set the servo pos
			_Rpos = round(_RA * sin(_phase + _phase0) + _O);
			_Rservo.write(_Rpos+90);
			_phase = _phase + _inc;

	  		if (dir * cos(_phase + _phase0) >= 0)
	  		{
				_Vservo.write(_down);
	  		}
	  		else
	  		{
				_Vservo.write(_down + _VA);
	  		}
		}
  	}
}

void Leg::step(int N)
{
	_phase = _phase + ((N-1) * _inc);
	_Rpos = round(_RA * sin(_phase + _phase0) + _O);
	_Rservo.write(_Rpos+90);

  	if (dir * cos(_phase + _phase0) > 0)
  	{
		_Vservo.write(_down);
  	}
  	else
  	{
		_Vservo.write(_down + _VA);
  	}
}

void Leg::refresh()
{
  	if (_state==1)
  	{
		walk();
  	}
  	else if (_state==2)
  	{
		stance();
  	}
  	else if(_state==3)
  	{
		stance();
  	}
}

void Leg::changeState(int state)
{
  	_state = state;
}

bool Leg::raised()
{
  	return (_Vservo.read() > _down);
}
