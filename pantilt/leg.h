
#ifndef Leg_h
#define Oscillator_h

#include <Servo.h>

//-- Macro for converting from degrees to radians
#ifndef DEG2RAD
  #define DEG2RAD(g) ((g)*M_PI)/180
#endif

class Leg
{
  public:
    Leg();
    void attach(int pinV, int pinR);
    void detach();
    void SetRA(unsigned int A) {_RA = A;};
    void SetVA(unsigned int A) {_VA = A;};
    void changeDir() {dir *= -1;};
    void SetDown(unsigned int D) {_down = D;};
    void SetO(unsigned int O) {_O=O;};
    void SetPh(double Ph) {_phase0=Ph;};
    void SetT(unsigned int T);
    void SetTS(unsigned int TS);
    void SetPosition(int positionV, int positionR);
    void Stop() {_stop=true;};
    void Go() {_stop=false;};
    void reset();
    void walk();
    void swing();
    void stance();
    void Up();
    void Down();
    void setInc();
    void step(int step);
    void refresh();
    void changeState(int state);
    bool raised();

    signed int dir;
    
  private:
    bool next_sample();  
    
  private:
    //-- Servo that is attached to the oscillator
    Servo _Vservo; //servo for vertical movement
    Servo _Rservo; //servo for rotation
    
    //-- Oscillators parameters
    unsigned int _state;
    unsigned int _down;
    unsigned int _RA;  //-- Rotational Amplitude (degrees)
    unsigned int _VA; //-- Vertical Amplitude
    unsigned int _O;  //-- Offset (degrees)
    unsigned int _T;  //-- Period (miliseconds)
    double _phase0;   //-- Phase (radians)
    
    //-- Internal variables
    int _Rpos;         //-- Current servo pos
    double _phase;    //-- Current phase
    double _inc;      //-- Increment of phase
    double _N;        //-- Number of samples
    unsigned int _TS; //-- sampling period (ms)
    
    long _previousMillis; 
    long _currentMillis;
    
    //-- Oscillation mode. If true, the servo is stopped
    bool _stop;

    //-- Reverse mode
    bool _rev;
};

#endif