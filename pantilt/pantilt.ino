#include <Servo.h>

Servo bottom;
Servo top;

int up_ang = 10;
int down_ang = 70;
int offset = 90;
int amp = 30;
int dp = 1;
int dir = 1;
int refresh = 10.;

int t0;
int t1;
int dt;
int pos = 0;


void setup() {
  bottom.attach(4);
  top.attach(5);
  t0 = millis();
}

void loop() {

  t1 = millis();
  dt = t1 - t0;

  if (dt >= refresh)
  {
    t0 = t1;
    pos = pos + (dp*dir);

    bottom.write(pos);

    if (pos==(offset+amp))
    {
      dir = -1;
      top.write(down_ang);
    }

    if (pos==(offset-amp))
    {
      dir = 1;
      top.write(up_ang);
    }
    
  }

}
